sudo apt-get update
sudo apt-get upgrade -y
sudo update-manager


# Git
sudo apt install git-all -y

# Visual Studio Code
sudo snap install --classic code

# Oh My Zsh
sudo apt install zsh
sudo apt-get install powerline fonts-powerline -y
git clone https://github.com/robbyrussell/oh-my-zsh.git ~/.oh-my-zsh
cp ~/.oh-my-zsh/templates/zshrc.zsh-template ~/.zshrc
chsh -s /bin/zsh # Manual insert password
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git "$HOME/.zsh-syntax-highlighting" --depth 1
echo "source $HOME/.zsh-syntax-highlighting/zsh-syntax-highlighting.zsh" >> "$HOME/.zshrc"

# Hyper Terminal
sudo apt-get install gconf-service-backend gconf2 gconf-service libappindicator1 -y
apt --fix-broken install -y
wget https://releases.hyper.is/download/deb
sudo dpkg -i deb
rm deb

# Nodejs/NPM/Yarn
curl -sL https://deb.nodesource.com/setup_13.x | sudo -E bash -
npm install -g yarn
mkdir ~/.npm-global
npm config set prefix '~/.npm-global'
echo 'export PATH=~/.npm-global/bin:$PATH' >> ~/.profile
source ~/.profile


# Docker
sudo apt install apt-transport-https ca-certificates curl software-properties-common -y
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"
sudo apt update
apt-cache policy docker-ce
sudo apt install docker-ce
sudo usermod -aG docker $USER

# Vivaldi
wget -qO- http://repo.vivaldi.com/stable/linux_signing_key.pub | sudo apt-key add -
sudo add-apt-repository "deb [arch=i386,amd64] http://repo.vivaldi.com/stable/deb/ stable main"
sudo npm install yarn -g

# Gloobus (Quicklook for Ubuntu)
sudo add-apt-repository ppa:gloobus-dev/gloobus-preview -y
sudo apt install vivaldi-stable

# Chromium
sudo snap install chromium

# Folder colours
sudo add-apt-repository ppa:costales/yaru-colors-folder-color
sudo apt install yaru-colors-folder-color

# Mirror Android smartphone
sudo snap install scrcpy
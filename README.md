# elementary-install

## First update the system
sudo apt-get update && sudo apt-get dist-upgrade

## When using it inside an Hyper-V virtual machine
sudo apt-get install hv-kvp-daemon-init

## Cleanup system


# Install browser(s)
sudo apt-get install firefox -y


## TODO


# Remove bloat
sudo rm /usr/share/applications/ubuntu-amazon-default.desktop
sudo apt-get purge aisleriot gnome-sudoku mahjongg ace-of-penguins gnomine gbrainy gnome-todo -y

# Settings
gsettings set org.gnome.desktop.wm.preferences button-layout 'close,maximize,minimize:'

# Install Ubuntu make to install 
# some development tools using command line
sudo add-apt-repository ppa:ubuntu-desktop/ubuntu-make -y
sudo apt-get install ubuntu-make -y

# (oh-my-)zsh - favourite theme is avit
sudo apt-get install wget curl git -y
sudo apt install zsh
chsh -s $(which zsh)

# Nodejs
curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -
sudo apt-get install -y nodejs
sudo apt-get install npm -y

# Visual Studio code
# Also install plugins using command line
umake ide visual-studio-code

# Keeweb
sudo wget -O - https://tagplus5.github.io/keeweb-ppa/ubuntu/gpg.key | sudo apt-key add - && \
sudo wget -O /etc/apt/sources.list.d/keeweb.list https://tagplus5.github.io/keeweb-ppa/ubuntu/keeweb.list && \
sudo apt install keeweb-desktop

# Utils
sudo apt install git-all -y
sudo apt-get install gnome-sushi -y # Mac's quicklook for Linux
sudo apt install fonts-firacode -y
sudo apt install chromium-browser -y
sudo add-apt-repository ppa:atareao/telegram -y && sudo apt-get install telegram -y
sudo apt install snapd
echo 'TODO: Install Keeweb'
echo 'TODO: Install VS Code'
